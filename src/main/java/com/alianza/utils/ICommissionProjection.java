package com.alianza.utils;

public interface ICommissionProjection {
    double getAmount();

    String getBuyer();

    String getAddress();

    double getPrice();

    String getCity();

    String getNeigh();

    String getName();

    String getLastName();
}
