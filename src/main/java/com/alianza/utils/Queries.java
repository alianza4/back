package com.alianza.utils;

public class Queries {
    public static final String commissions =
            "SELECT " +
                    "co.amount AS amount, " +
                    "co.buyer AS buyer, " +
                    "p.address AS address, " +
                    "p.price AS price, " +
                    "ci.description AS city, " +
                    "ne.description AS neigh, " +
                    "us.name AS name, " +
                    "us.last_name AS lastName, " +
                    "us.phone AS phone, " +
                    "us.email AS email " +
                    "FROM commission AS co " +
                    "INNER JOIN property AS p ON " +
                    "co.id_property = p.id " +
                    "INNER JOIN city AS ci ON p.id_city = ci.id " +
                    "INNER JOIN neighbourhood AS ne ON p.id_neighbourhood = ne.id " +
                    "INNER JOIN user AS us ON " +
                    "co.id_user = us.id;";

    public static final String commissionsByUser =
            "SELECT " +
                    "co.amount AS amount, " +
                    "co.buyer AS buyer, " +
                    "p.address AS address, " +
                    "p.price AS price, " +
                    "ci.description AS city, " +
                    "ne.description AS neigh, " +
                    "us.name AS name, " +
                    "us.last_name AS lastName, " +
                    "us.phone AS phone, " +
                    "us.email AS email " +
                    "FROM commission AS co " +
                    "INNER JOIN property AS p ON " +
                    "co.id_property = p.id " +
                    "INNER JOIN city AS ci ON p.id_city = ci.id " +
                    "INNER JOIN neighbourhood AS ne ON p.id_neighbourhood = ne.id " +
                    "INNER JOIN user AS us ON " +
                    "co.id_user = us.id " +
                    "where us.id = :id";
}
