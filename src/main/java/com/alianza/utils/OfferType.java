package com.alianza.utils;

public enum OfferType {
    SALE,
    RENTAL
}
