package com.alianza.controllers;

import com.alianza.domain.TypePropertyService;
import com.alianza.domain.dto.TypePropertyDTO;
import com.alianza.response.RequestResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/typeProperty")
public class TypePropertyController {
    private final TypePropertyService _typePropertyService;

    @Autowired
    public TypePropertyController(TypePropertyService typePropertyService) {
        _typePropertyService = typePropertyService;
    }

    @GetMapping("/getAll")
    public ResponseEntity<RequestResult<List<TypePropertyDTO>>> getAll() {
        return _typePropertyService.getAll();
    }
}
