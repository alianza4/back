package com.alianza.controllers;

import com.alianza.domain.UserService;
import com.alianza.domain.dto.RequestLoginDTO;
import com.alianza.domain.dto.RolDTO;
import com.alianza.domain.dto.UserDTO;
import com.alianza.domain.dto.UserTokenDTO;
import com.alianza.response.RequestResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/user")
public class UserController {
    private final UserService _userService;

    @Autowired
    public UserController(UserService userService) {
        _userService = userService;
    }

    @GetMapping("/getAll")
    public ResponseEntity<RequestResult<List<UserDTO>>> getAll() {
        return _userService.getAll();
    }

    @GetMapping("/getAllRoles")
    public ResponseEntity<RequestResult<List<RolDTO>>> getAllRoles() {
        return _userService.getAllRoles();
    }

    @GetMapping("/get")
    public ResponseEntity<RequestResult<UserDTO>> get(@RequestParam String id) {
        return _userService.get(id);
    }

    @PostMapping("/save")
    public ResponseEntity<RequestResult<UserTokenDTO>> save(@RequestBody UserDTO userDTO) {
        return _userService.save(userDTO);
    }

    @CrossOrigin(origins = "http://localhost:4200")
    @PostMapping("/update")
    public ResponseEntity<RequestResult<UserDTO>> update(@RequestBody UserDTO userDTO) {
        return _userService.update(userDTO);
    }

    @CrossOrigin(origins = "http://localhost:4200")
    @DeleteMapping("/delete")
    public ResponseEntity<RequestResult<Boolean>> delete(@RequestParam String id) {
        return _userService.delete(id);
    }

    @PostMapping("/login")
    public ResponseEntity<RequestResult<UserTokenDTO>> login(@RequestBody RequestLoginDTO loginDTO) {
        return _userService.login(loginDTO);
    }

    @PostMapping("/getComercial")
    public ResponseEntity<RequestResult<UserDTO>> getComercial(@RequestBody List<String> ids) {
        return _userService.getUserComercial(ids);
    }
}
