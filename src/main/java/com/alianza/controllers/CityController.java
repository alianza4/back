package com.alianza.controllers;

import com.alianza.domain.CityService;
import com.alianza.domain.dto.CityDTO;
import com.alianza.response.RequestResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/city")
public class CityController {
    private final CityService _cityService;

    @Autowired
    public CityController(CityService cityService) {
        _cityService = cityService;
    }

    @GetMapping("/getAll")
    public ResponseEntity<RequestResult<List<CityDTO>>> getAll() {
        return _cityService.getAll();
    }
}
