package com.alianza.controllers;

import com.alianza.domain.NeighbourhoodService;
import com.alianza.domain.dto.NeighbourhoodDTO;
import com.alianza.response.RequestResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/neighbourhood")
public class NeighbourhoodController {
    private final NeighbourhoodService _neighbourhoodService;

    @Autowired
    public NeighbourhoodController(NeighbourhoodService neighbourhoodService) {
        _neighbourhoodService = neighbourhoodService;
    }

    @GetMapping("/getByCity")
    public ResponseEntity<RequestResult<List<NeighbourhoodDTO>>> getAllByCity(@RequestParam String idCity) {
        return _neighbourhoodService.getByCity(idCity);
    }
}
