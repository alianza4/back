package com.alianza.controllers;

import com.alianza.domain.PropertyService;
import com.alianza.domain.dto.PropertyDTO;
import com.alianza.response.RequestResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/property")
public class PropertyController {
    private final PropertyService _propertyService;

    @Autowired
    public PropertyController(PropertyService propertyService) {
        _propertyService = propertyService;
    }

    @GetMapping("/getAll")
    public ResponseEntity<RequestResult<List<PropertyDTO>>> getAll() {
        return _propertyService.getAll();
    }

    @GetMapping("/getByUser")
    public ResponseEntity<RequestResult<List<PropertyDTO>>> getPropertiesByUser(@RequestParam String id) {
        return _propertyService.getPropertiesByUser(id);
    }

    @GetMapping("/get")
    public ResponseEntity<RequestResult<PropertyDTO>> get(@RequestParam String id) {
        return _propertyService.get(id);
    }

    @PostMapping("/save")
    public ResponseEntity<RequestResult<PropertyDTO>> save(@RequestBody PropertyDTO propertyDTO) {
        return _propertyService.save(propertyDTO);
    }

    @PutMapping("/update")
    public ResponseEntity<RequestResult<PropertyDTO>> update(@RequestParam String id, @RequestBody PropertyDTO propertyDTO) {
        return _propertyService.update(id, propertyDTO);
    }

    @CrossOrigin(origins = "http://localhost:4200")
    @DeleteMapping("/delete")
    public ResponseEntity<RequestResult<Boolean>> delete(@RequestParam String id) {
        return _propertyService.delete(id);
    }
}
