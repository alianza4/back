package com.alianza.controllers;

import com.alianza.domain.CommissionService;
import com.alianza.domain.dto.CommissionDTO;
import com.alianza.response.RequestResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/commission")
public class CommissionController {
    private final CommissionService _commissionService;

    @Autowired
    public CommissionController(CommissionService commissionService) {
        _commissionService = commissionService;
    }

    @PostMapping("/sale")
    public ResponseEntity<RequestResult<CommissionDTO>> sale(@RequestBody CommissionDTO commissionDTO) {
        return _commissionService.sale(commissionDTO);
    }

    @GetMapping("/getCommissions")
    public ResponseEntity<RequestResult<List<CommissionDTO>>> getCommissionsByUser(@RequestParam String idUser) {
        return _commissionService.getCommissionsByUser(idUser);
    }

    @GetMapping("/export")
    public byte[] exportReport(@RequestParam String id) {
        var result = _commissionService.exportReport(id);
        if (result.getBody().isSuccessful())
            return result.getBody().getResult();

        return null;
    }
}
