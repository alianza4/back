package com.alianza.response;

import java.util.ArrayList;
import java.util.List;

public class RequestResult<T> {

    private boolean isSuccessful;
    private boolean isError;
    private String errorMessage;
    private List<String> messages;
    private T result;

    public RequestResult() {
    }

    public RequestResult(boolean isSuccessful, boolean isError, String errorMessage, List<String> messages, T result) {
        this.isSuccessful = isSuccessful;
        this.isError = isError;
        this.errorMessage = errorMessage;
        this.messages = messages;
        this.result = result;
    }

    public RequestResult<T> CreateSuccessful(T result) {
        RequestResult<T> requestResult = new RequestResult<T>(true, false, "", new ArrayList<>(), result);
        return requestResult;
    }

    public RequestResult<T> CreateUnsuccessful(List<String> messages) {
        RequestResult<T> requestResult = new RequestResult<T>(false, false, "", messages, null);
        return requestResult;
    }

    public RequestResult<T> CreateError(String errorMessage) {
        RequestResult<T> requestResult = new RequestResult<T>(false, true, errorMessage, new ArrayList<>(), null);
        return requestResult;
    }

    public RequestResult<T> CreateEntityNotExists(String entityId) {
        ArrayList<String> messages = new ArrayList<>() {
            {
                add("La entidad con ID " + entityId + " no ha sido encontrada");
            }
        };

        RequestResult<T> requestResult = new RequestResult<T>(false, false, "", messages, null);
        return requestResult;
    }

    public boolean isSuccessful() {
        return isSuccessful;
    }

    public void setSuccessful(boolean successful) {
        isSuccessful = successful;
    }

    public boolean isError() {
        return isError;
    }

    public void setError(boolean error) {
        isError = error;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public List<String> getMessages() {
        return messages;
    }

    public void setMessages(List<String> messages) {
        this.messages = messages;
    }

    public T getResult() {
        return result;
    }

    public void setResult(T result) {
        this.result = result;
    }
}
