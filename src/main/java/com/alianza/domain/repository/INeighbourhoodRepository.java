package com.alianza.domain.repository;

import com.alianza.domain.dto.NeighbourhoodDTO;

import java.util.List;

public interface INeighbourhoodRepository {
    List<NeighbourhoodDTO> getAllByCity(String idCity);
}
