package com.alianza.domain.repository;

import com.alianza.domain.dto.RequestLoginDTO;
import com.alianza.domain.dto.RolDTO;
import com.alianza.domain.dto.UserDTO;

import java.util.List;

public interface IUserRepository {
    List<UserDTO> getAll();

    UserDTO get(String id);

    UserDTO save(UserDTO userDTO);

    boolean delete(String id);

    UserDTO login(RequestLoginDTO requestLoginDTO);

    List<RolDTO> getAllRoles();

    UserDTO getUserComercial(List<String> ids);
}
