package com.alianza.domain.repository;

import com.alianza.domain.dto.TypePropertyDTO;

import java.util.List;

public interface ITypePropertyRepository {
    List<TypePropertyDTO> getAll();
}
