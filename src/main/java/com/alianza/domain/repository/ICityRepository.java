package com.alianza.domain.repository;

import com.alianza.domain.dto.CityDTO;

import java.util.List;

public interface ICityRepository {
    List<CityDTO> getAll();
}
