package com.alianza.domain.repository;

import com.alianza.domain.dto.CommissionDTO;
import com.alianza.domain.dto.CommissionReportDTO;

import java.util.List;

public interface ICommissionRepository {
    CommissionDTO sale(CommissionDTO commissionDTO);

    List<CommissionDTO> getCommissionsByUser(String idUser);

    List<CommissionReportDTO> findAllCommissions(String id);
}
