package com.alianza.domain.repository;

import com.alianza.domain.dto.PropertyDTO;

import java.util.List;

public interface IPropertyRepository {
    List<PropertyDTO> getAll();

    PropertyDTO get(String id);

    PropertyDTO save(PropertyDTO propertyDTO);

    boolean delete(String id);

    List<PropertyDTO> getPropertiesByUser(String id);
}
