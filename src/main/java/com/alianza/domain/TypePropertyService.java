package com.alianza.domain;

import com.alianza.domain.dto.TypePropertyDTO;
import com.alianza.domain.repository.ITypePropertyRepository;
import com.alianza.response.RequestResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TypePropertyService {
    private final ITypePropertyRepository _propertyRepository;

    @Autowired
    public TypePropertyService(ITypePropertyRepository propertyRepository) {
        _propertyRepository = propertyRepository;
    }

    public ResponseEntity<RequestResult<List<TypePropertyDTO>>> getAll() {
        RequestResult<List<TypePropertyDTO>> requestResult = new RequestResult<>();
        try {
            List<TypePropertyDTO> typePropertyDTOS = _propertyRepository.getAll();
            return new ResponseEntity<>(requestResult.CreateSuccessful(typePropertyDTOS), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(requestResult.CreateError(e.getMessage()), HttpStatus.OK);
        }
    }
}
