package com.alianza.domain;

import com.alianza.domain.dto.NeighbourhoodDTO;
import com.alianza.domain.repository.INeighbourhoodRepository;
import com.alianza.response.RequestResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class NeighbourhoodService {
    private final INeighbourhoodRepository _neighbourhoodRepository;

    @Autowired
    public NeighbourhoodService(INeighbourhoodRepository neighbourhoodRepository) {
        _neighbourhoodRepository = neighbourhoodRepository;
    }

    public ResponseEntity<RequestResult<List<NeighbourhoodDTO>>> getByCity(String idCity) {
        RequestResult<List<NeighbourhoodDTO>> requestResult = new RequestResult<>();
        try {
            List<NeighbourhoodDTO> neighbourhoodDTOS = _neighbourhoodRepository.getAllByCity(idCity);
            return new ResponseEntity<>(requestResult.CreateSuccessful(neighbourhoodDTOS), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(requestResult.CreateError(e.getMessage()), HttpStatus.OK);
        }
    }
}
