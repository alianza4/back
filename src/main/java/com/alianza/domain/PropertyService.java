package com.alianza.domain;

import com.alianza.domain.dto.PropertyDTO;
import com.alianza.domain.repository.IPropertyRepository;
import com.alianza.response.RequestResult;
import com.google.common.base.Strings;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

@Service
public class PropertyService {
    private final IPropertyRepository _propertyRepository;

    @Autowired
    public PropertyService(IPropertyRepository propertyRepository) {
        _propertyRepository = propertyRepository;
    }

    public ResponseEntity<RequestResult<List<PropertyDTO>>> getAll() {
        RequestResult<List<PropertyDTO>> requestResult = new RequestResult<List<PropertyDTO>>();
        try {
            List<PropertyDTO> propertyDTOS = _propertyRepository.getAll();
            return new ResponseEntity<>(requestResult.CreateSuccessful(propertyDTOS), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(requestResult.CreateError(e.getMessage()), HttpStatus.OK);
        }
    }

    public ResponseEntity<RequestResult<List<PropertyDTO>>> getPropertiesByUser(String id) {
        RequestResult<List<PropertyDTO>> requestResult = new RequestResult<List<PropertyDTO>>();
        try {
            List<PropertyDTO> propertyDTOS = _propertyRepository.getPropertiesByUser(id);
            return new ResponseEntity<>(requestResult.CreateSuccessful(propertyDTOS), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(requestResult.CreateError(e.getMessage()), HttpStatus.OK);
        }
    }

    public ResponseEntity<RequestResult<PropertyDTO>> get(String id) {
        RequestResult<PropertyDTO> requestResult = new RequestResult<PropertyDTO>();
        try {
            PropertyDTO propertyDTO = _propertyRepository.get(id);
            if (propertyDTO != null) {
                return new ResponseEntity<>(requestResult.CreateSuccessful(propertyDTO), HttpStatus.OK);
            }

            return new ResponseEntity<>(requestResult.CreateEntityNotExists(id), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(requestResult.CreateError(e.getMessage()), HttpStatus.OK);
        }
    }

    public ResponseEntity<RequestResult<PropertyDTO>> save(PropertyDTO propertyDTO) {
        RequestResult<PropertyDTO> requestResult = new RequestResult<>();
        try {
            String id = UUID.randomUUID().toString();
            if (Strings.isNullOrEmpty(propertyDTO.getId().trim()))
                propertyDTO.setId(id);
            PropertyDTO property = _propertyRepository.save(propertyDTO);
            return new ResponseEntity<>(requestResult.CreateSuccessful(property), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(requestResult.CreateError(e.getMessage()), HttpStatus.OK);
        }
    }

    public ResponseEntity<RequestResult<Boolean>> delete(String id) {
        RequestResult<Boolean> requestResult = new RequestResult<Boolean>();
        try {
            if (_propertyRepository.delete(id)) {
                return new ResponseEntity<>(requestResult.CreateSuccessful(true), HttpStatus.OK);
            }

            return new ResponseEntity<>(requestResult.CreateEntityNotExists(id), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(requestResult.CreateError(e.getMessage()), HttpStatus.OK);
        }
    }

    public ResponseEntity<RequestResult<PropertyDTO>> update(String id, PropertyDTO propertyDTO) {
        RequestResult<PropertyDTO> requestResult = new RequestResult<PropertyDTO>();
        try {
            if (_propertyRepository.get(id) != null) {
                PropertyDTO property = _propertyRepository.save(propertyDTO);
                return new ResponseEntity<>(requestResult.CreateSuccessful(property), HttpStatus.OK);
            }

            return new ResponseEntity<>(requestResult.CreateEntityNotExists(id), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(requestResult.CreateError(e.getMessage()), HttpStatus.OK);
        }
    }
}
