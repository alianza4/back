package com.alianza.domain.dto;

public class CommissionReportDTO {
    private double amount;
    private String buyer;
    private String address;
    private double price;
    private String city;
    private String neigh;
    private String name;
    private String lastName;

    public CommissionReportDTO(double amount, String buyer, String address, double price, String city, String neigh, String name, String lastName) {
        this.amount = amount;
        this.buyer = buyer;
        this.address = address;
        this.price = price;
        this.city = city;
        this.neigh = neigh;
        this.name = name;
        this.lastName = lastName;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public String getBuyer() {
        return buyer;
    }

    public void setBuyer(String buyer) {
        this.buyer = buyer;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getNeigh() {
        return neigh;
    }

    public void setNeigh(String neigh) {
        this.neigh = neigh;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }
}
