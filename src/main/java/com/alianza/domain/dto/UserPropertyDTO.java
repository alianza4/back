package com.alianza.domain.dto;

public class UserPropertyDTO {
    private String idUser;
    private String idProperty;

    public UserPropertyDTO() {
    }

    public UserPropertyDTO(String idUser, String idProperty) {
        this.idUser = idUser;
        this.idProperty = idProperty;
    }

    public String getIdProperty() {
        return idProperty;
    }

    public void setIdProperty(String idProperty) {
        this.idProperty = idProperty;
    }

    public String getIdUser() {
        return idUser;
    }

    public void setIdUser(String idUser) {
        this.idUser = idUser;
    }
}
