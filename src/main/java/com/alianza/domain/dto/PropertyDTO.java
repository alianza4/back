package com.alianza.domain.dto;

import com.alianza.utils.OfferType;
import com.alianza.utils.StateProperty;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import java.util.List;

public class PropertyDTO {
    private String id;
    @Enumerated(EnumType.ORDINAL)
    private OfferType offerType;
    private String idTypeProperty;
    private String idCity;
    private String idNeighbourhood;
    private String address;
    private double price;
    private int stratum;
    private double area;
    private boolean administrationInPrice;
    private int rooms;
    private int bathrooms;
    private int floors;
    private String description;
    private String idImage;
    private boolean parking;
    private CityDTO city;
    private NeighbourhoodDTO neighbourhood;
    private ImageDTO image;
    private TypePropertyDTO typeProperty;
    private List<UserPropertyDTO> userProperties;
    @Enumerated(EnumType.ORDINAL)
    private StateProperty state = StateProperty.OFFER;

    public List<UserPropertyDTO> getUserProperties() {
        return userProperties;
    }

    public void setUserProperties(List<UserPropertyDTO> userProperties) {
        this.userProperties = userProperties;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public OfferType getOfferType() {
        return offerType;
    }

    public void setOfferType(OfferType offerType) {
        this.offerType = offerType;
    }

    public String getIdTypeProperty() {
        return idTypeProperty;
    }

    public void setIdTypeProperty(String idTypeProperty) {
        this.idTypeProperty = idTypeProperty;
    }

    public String getIdCity() {
        return idCity;
    }

    public void setIdCity(String idCity) {
        this.idCity = idCity;
    }

    public String getIdNeighbourhood() {
        return idNeighbourhood;
    }

    public void setIdNeighbourhood(String idNeighbourhood) {
        this.idNeighbourhood = idNeighbourhood;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public int getStratum() {
        return stratum;
    }

    public void setStratum(int stratum) {
        this.stratum = stratum;
    }

    public double getArea() {
        return area;
    }

    public void setArea(double area) {
        this.area = area;
    }

    public boolean isAdministrationInPrice() {
        return administrationInPrice;
    }

    public void setAdministrationInPrice(boolean administrationInPrice) {
        this.administrationInPrice = administrationInPrice;
    }

    public int getRooms() {
        return rooms;
    }

    public void setRooms(int rooms) {
        this.rooms = rooms;
    }

    public int getBathrooms() {
        return bathrooms;
    }

    public void setBathrooms(int bathrooms) {
        this.bathrooms = bathrooms;
    }

    public int getFloors() {
        return floors;
    }

    public void setFloors(int floors) {
        this.floors = floors;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getIdImage() {
        return idImage;
    }

    public void setIdImage(String idImage) {
        this.idImage = idImage;
    }

    public boolean isParking() {
        return parking;
    }

    public void setParking(boolean parking) {
        this.parking = parking;
    }

    public CityDTO getCity() {
        return city;
    }

    public void setCity(CityDTO city) {
        this.city = city;
    }

    public NeighbourhoodDTO getNeighbourhood() {
        return neighbourhood;
    }

    public void setNeighbourhood(NeighbourhoodDTO neighbourhood) {
        this.neighbourhood = neighbourhood;
    }

    public ImageDTO getImage() {
        return image;
    }

    public void setImage(ImageDTO image) {
        this.image = image;
    }

    public TypePropertyDTO getTypeProperty() {
        return typeProperty;
    }

    public void setTypeProperty(TypePropertyDTO typeProperty) {
        this.typeProperty = typeProperty;
    }

    public StateProperty getState() {
        return state;
    }

    public void setState(StateProperty state) {
        this.state = state;
    }
}
