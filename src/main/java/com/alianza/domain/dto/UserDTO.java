package com.alianza.domain.dto;

import java.util.List;

public class UserDTO {
    private String id;
    private String name;
    private String lastName;
    private String phone;
    private String email;
    private String password;
    private String idRol;
    private RolDTO rol;
    private List<UserPropertyDTO> userProperties;

    public List<UserPropertyDTO> getUserProperties() {
        return userProperties;
    }

    public void setUserProperties(List<UserPropertyDTO> userProperties) {
        this.userProperties = userProperties;
    }

    public String getIdRol() {
        return idRol;
    }

    public void setIdRol(String idRol) {
        this.idRol = idRol;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public RolDTO getRol() {
        return rol;
    }

    public void setRol(RolDTO rol) {
        this.rol = rol;
    }
}
