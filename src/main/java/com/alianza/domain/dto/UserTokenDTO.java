package com.alianza.domain.dto;

import java.util.Date;

public class UserTokenDTO {
    private String id;
    private String name;
    private String lastName;
    private String phone;
    private String email;
    private String password;
    private String idRol;
    private RolDTO rol;
    private String token;
    private Date expiration;

    public UserTokenDTO(UserDTO userDTO, String token, Date expiration) {
        id = userDTO.getId();
        name = userDTO.getName();
        lastName = userDTO.getLastName();
        phone = userDTO.getPhone();
        email = userDTO.getEmail();
        password = userDTO.getPassword();
        idRol = userDTO.getIdRol();
        rol = userDTO.getRol();
        this.token = token;
        this.expiration = expiration;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getIdRol() {
        return idRol;
    }

    public void setIdRol(String idRol) {
        this.idRol = idRol;
    }

    public RolDTO getRol() {
        return rol;
    }

    public void setRol(RolDTO rol) {
        this.rol = rol;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public Date getExpiration() {
        return expiration;
    }

    public void setExpiration(Date expiration) {
        this.expiration = expiration;
    }
}
