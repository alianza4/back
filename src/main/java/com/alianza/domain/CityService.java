package com.alianza.domain;

import com.alianza.domain.dto.CityDTO;
import com.alianza.domain.repository.ICityRepository;
import com.alianza.response.RequestResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CityService {
    private final ICityRepository _cityRepository;

    @Autowired
    public CityService(ICityRepository cityRepository) {
        _cityRepository = cityRepository;
    }

    public ResponseEntity<RequestResult<List<CityDTO>>> getAll() {
        RequestResult<List<CityDTO>> requestResult = new RequestResult<>();
        try {
            List<CityDTO> cityDTOS = _cityRepository.getAll();
            return new ResponseEntity<>(requestResult.CreateSuccessful(cityDTOS), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(requestResult.CreateError(e.getMessage()), HttpStatus.OK);
        }
    }
}
