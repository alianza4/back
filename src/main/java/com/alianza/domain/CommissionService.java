package com.alianza.domain;

import com.alianza.domain.dto.CommissionDTO;
import com.alianza.domain.dto.CommissionReportDTO;
import com.alianza.domain.dto.UserDTO;
import com.alianza.domain.dto.UserTokenDTO;
import com.alianza.domain.repository.ICommissionRepository;
import com.alianza.response.RequestResult;
import net.sf.jasperreports.engine.*;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.ResourceUtils;

import java.io.File;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

@Service
public class CommissionService {
    private final ICommissionRepository _commissionRepository;

    @Autowired
    public CommissionService(ICommissionRepository commissionRepository) {
        _commissionRepository = commissionRepository;
    }

    public ResponseEntity<RequestResult<CommissionDTO>> sale(CommissionDTO commissionDTO) {
        RequestResult<CommissionDTO> requestResult = new RequestResult<>();
        try {
            String id = UUID.randomUUID().toString();
            commissionDTO.setId(id);
            CommissionDTO commission = _commissionRepository.sale(commissionDTO);
            return new ResponseEntity<>(requestResult.CreateSuccessful(commission), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(requestResult.CreateError(e.getMessage()), HttpStatus.OK);
        }
    }

    public ResponseEntity<RequestResult<List<CommissionDTO>>> getCommissionsByUser(String idUser) {
        RequestResult<List<CommissionDTO>> requestResult = new RequestResult<>();
        try {
            List<CommissionDTO> commissions = _commissionRepository.getCommissionsByUser(idUser);
            return new ResponseEntity<>(requestResult.CreateSuccessful(commissions), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(requestResult.CreateError(e.getMessage()), HttpStatus.OK);
        }
    }

    public ResponseEntity<RequestResult<byte[]>> exportReport(String id) {
        RequestResult<byte[]> requestResult = new RequestResult<>();
        try {
            var commissions = _commissionRepository.findAllCommissions(id);

            // Load and compile it
            File file = ResourceUtils.getFile("classpath:JasperReport/commissionCopy.jrxml");
            JasperReport jasperReport = JasperCompileManager.compileReport(file.getAbsolutePath());
            JRBeanCollectionDataSource dataSource = new JRBeanCollectionDataSource(commissions);
            Map<String, Object> parameters = new HashMap<>();
            parameters.put("createdBy", "Alianza Inmobiliaria");
            JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, dataSource);
            var fileByte = JasperExportManager.exportReportToPdf(jasperPrint);

            return new ResponseEntity<>(requestResult.CreateSuccessful(fileByte), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(requestResult.CreateError(e.getMessage()), HttpStatus.OK);
        }
    }
}
