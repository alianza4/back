package com.alianza.domain;

import com.alianza.domain.dto.RequestLoginDTO;
import com.alianza.domain.dto.RolDTO;
import com.alianza.domain.dto.UserDTO;
import com.alianza.domain.dto.UserTokenDTO;
import com.alianza.domain.repository.IUserRepository;
import com.alianza.persistence.entity.User;
import com.alianza.response.RequestResult;
import com.google.common.base.Strings;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class UserService {
    private final IUserRepository _userRepository;

    @Autowired
    public UserService(IUserRepository userRepository) {
        _userRepository = userRepository;
    }

    public ResponseEntity<RequestResult<List<UserDTO>>> getAll() {
        RequestResult<List<UserDTO>> requestResult = new RequestResult<>();
        try {
            List<UserDTO> users = _userRepository.getAll();
            return new ResponseEntity<>(requestResult.CreateSuccessful(users), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(requestResult.CreateError(e.getMessage()), HttpStatus.OK);
        }
    }

    public ResponseEntity<RequestResult<UserDTO>> get(String id) {
        RequestResult<UserDTO> requestResult = new RequestResult<>();
        try {
            UserDTO user = _userRepository.get(id);
            if (user != null) {
                return new ResponseEntity<>(requestResult.CreateSuccessful(user), HttpStatus.OK);
            }

            return new ResponseEntity<>(requestResult.CreateEntityNotExists(id), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(requestResult.CreateError(e.getMessage()), HttpStatus.OK);
        }
    }

    public ResponseEntity<RequestResult<Boolean>> delete(String id) {
        RequestResult<Boolean> requestResult = new RequestResult<Boolean>();
        try {
            if (_userRepository.delete(id)) {
                return new ResponseEntity<>(requestResult.CreateSuccessful(true), HttpStatus.OK);
            }

            return new ResponseEntity<>(requestResult.CreateEntityNotExists(id), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(requestResult.CreateError(e.getMessage()), HttpStatus.OK);
        }
    }

    public ResponseEntity<RequestResult<UserDTO>> update(UserDTO userDTO) {
        RequestResult<UserDTO> requestResult = new RequestResult<>();
        try {
            String id = UUID.randomUUID().toString();
            if (Strings.isNullOrEmpty(userDTO.getId().trim()))
                userDTO.setId(id);

            UserDTO user = _userRepository.save(userDTO);
            return new ResponseEntity<>(requestResult.CreateSuccessful(user), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(requestResult.CreateError(e.getMessage()), HttpStatus.OK);
        }
    }

    public ResponseEntity<RequestResult<UserTokenDTO>> save(UserDTO userDTO) {
        RequestResult<UserTokenDTO> requestResult = new RequestResult<>();
        try {
            String id = UUID.randomUUID().toString();
            userDTO.setId(id);
            UserDTO user = _userRepository.save(userDTO);
            return new ResponseEntity<>(requestResult.CreateSuccessful(getJWTToken(user)), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(requestResult.CreateError(e.getMessage()), HttpStatus.OK);
        }
    }

    public ResponseEntity<RequestResult<UserTokenDTO>> login(RequestLoginDTO requestLoginDTO) {
        RequestResult<UserTokenDTO> requestResult = new RequestResult<>();
        try {
            UserDTO user = _userRepository.login(requestLoginDTO);
            if (user != null) {
                UserTokenDTO userToken = getJWTToken(user);
                return new ResponseEntity<>(requestResult.CreateSuccessful(userToken), HttpStatus.OK);
            }

            return new ResponseEntity<>(requestResult.CreateUnsuccessful(new ArrayList<>() {{
                add("El usuario o la contraseña son inavalidas");
            }}), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(requestResult.CreateError(e.getMessage()), HttpStatus.OK);
        }
    }

    public ResponseEntity<RequestResult<List<RolDTO>>> getAllRoles() {
        RequestResult<List<RolDTO>> requestResult = new RequestResult<>();
        try {
            List<RolDTO> roles = _userRepository.getAllRoles();
            return new ResponseEntity<>(requestResult.CreateSuccessful(roles), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(requestResult.CreateError(e.getMessage()), HttpStatus.OK);
        }
    }

    public ResponseEntity<RequestResult<UserDTO>> getUserComercial(List<String> ids) {
        RequestResult<UserDTO> requestResult = new RequestResult<>();
        try {
            UserDTO userDTO = _userRepository.getUserComercial(ids);
            if (userDTO == null)
                return new ResponseEntity<>(requestResult.CreateUnsuccessful(new ArrayList<>() {{
                    add("No se encontraron comerciales en los ids enviados ");
                }}), HttpStatus.OK);

            return new ResponseEntity<>(requestResult.CreateSuccessful(userDTO), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(requestResult.CreateError(e.getMessage()), HttpStatus.OK);
        }
    }

    private UserTokenDTO getJWTToken(UserDTO user) {
        String secretKey = "4li4az#$P00LiGR44at";

        List<GrantedAuthority> grantedAuthorities = AuthorityUtils.commaSeparatedStringToAuthorityList(user.getRol() != null ? user.getRol().getDescription() : "Usuario");

        String token = Jwts
                .builder()
                .setSubject(user.getEmail())
                .claim(
                        "authorities",
                        grantedAuthorities
                                .stream()
                                .map(GrantedAuthority::getAuthority)
                                .collect(Collectors.toList()))
                .setIssuedAt(new Date())
                .setExpiration(new Date(System.currentTimeMillis() + 1000 * 60 * 60 * 10))
                .signWith(SignatureAlgorithm.HS256, secretKey).compact();

        UserTokenDTO userToken = new UserTokenDTO(user, "Bearer " + token, new Date(System.currentTimeMillis() + 1000 * 60 * 60 * 10));
        return userToken;
    }
}
