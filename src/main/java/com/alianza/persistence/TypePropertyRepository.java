package com.alianza.persistence;

import com.alianza.domain.dto.TypePropertyDTO;
import com.alianza.domain.repository.ITypePropertyRepository;
import com.alianza.persistence.entity.TypeProperty;
import com.alianza.persistence.mapper.TypePropertyMapper;
import com.alianza.persistence.operationsDB.TypePropertyDBRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class TypePropertyRepository implements ITypePropertyRepository {
    private final TypePropertyDBRepository _repository;
    private final TypePropertyMapper _mapper;

    @Autowired
    public TypePropertyRepository(TypePropertyDBRepository repository, TypePropertyMapper mapper) {
        _repository = repository;
        _mapper = mapper;
    }

    @Override
    public List<TypePropertyDTO> getAll() {
        List<TypeProperty> typeProperties = (List<TypeProperty>) _repository.findAll();
        return _mapper.toTypePropertiesDTO(typeProperties);
    }
}
