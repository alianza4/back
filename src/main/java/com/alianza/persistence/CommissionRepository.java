package com.alianza.persistence;

import com.alianza.domain.dto.CommissionDTO;
import com.alianza.domain.dto.CommissionReportDTO;
import com.alianza.domain.repository.ICommissionRepository;
import com.alianza.persistence.entity.Commission;
import com.alianza.persistence.entity.User;
import com.alianza.persistence.mapper.CommissionMapper;
import com.alianza.persistence.operationsDB.CommissionDBRepository;
import com.alianza.persistence.operationsDB.PropertyDBRepository;
import com.alianza.persistence.operationsDB.UserDBRepository;
import com.alianza.utils.ICommissionProjection;
import com.alianza.utils.StateProperty;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;

@Repository
public class CommissionRepository implements ICommissionRepository {
    private final CommissionDBRepository _commissionDBRepository;
    private final PropertyDBRepository _propertyDBRepository;
    private final UserDBRepository _userDBRepository;
    private final CommissionMapper _mapper;

    @Autowired
    public CommissionRepository(CommissionDBRepository commissionDBRepository, CommissionMapper mapper, PropertyDBRepository propertyDBRepository, UserDBRepository userDBRepository) {
        _commissionDBRepository = commissionDBRepository;
        _mapper = mapper;
        _propertyDBRepository = propertyDBRepository;
        _userDBRepository = userDBRepository;
    }

    @Override
    public CommissionDTO sale(CommissionDTO commissionDTO) {
        var property = _propertyDBRepository.findById(commissionDTO.getIdProperty()).map(p -> p).orElse(null);
        AtomicReference<String> id = new AtomicReference<>("");
        property.getUserProperties().forEach(prop -> {
            var user = _userDBRepository.findById(prop.getId().getIdUser()).map(u -> u).orElse(null);
            if (user != null) {
                if (user.getIdRol().equals("41f1680d-f31e-48b7-8d29-18be803641ad")) {
                    id.set(user.getId());
                }
            }
        });

        commissionDTO.setIdUser(id.get());
        Commission commission = _mapper.toCommission(commissionDTO);
        property.setUserProperties(new ArrayList<>());
        property.setState(StateProperty.SALE);
        _propertyDBRepository.save(property);
        return _mapper.toCommissionDTO(_commissionDBRepository.save(commission));
    }

    @Override
    public List<CommissionDTO> getCommissionsByUser(String idUser) {
        User user = _userDBRepository.findById(idUser).map(u -> u).orElse(null);
        if (user.getIdRol().equals("01fd1387-74a4-42ac-a51f-3b7e1d3328bd")) {
            List<Commission> commissions = (List<Commission>) _commissionDBRepository.findAll();
            return _mapper.toCommissionsDTO(commissions);
        }

        List<Commission> commissions = _commissionDBRepository.findByIdUser(idUser).map(cms -> cms).orElse(null);
        if (commissions == null)
            return null;

        return _mapper.toCommissionsDTO(commissions);
    }

    @Override
    public List<CommissionReportDTO> findAllCommissions(String id) {
        ArrayList<CommissionReportDTO> comissionsReport = new ArrayList<>();

        User user = _userDBRepository.findById(id).map(u -> u).orElse(null);
        List<ICommissionProjection> commissions = new ArrayList<>();

        if (user.getIdRol().equals("01fd1387-74a4-42ac-a51f-3b7e1d3328bd")) {
            commissions = _commissionDBRepository.findByCommissionsAll();
        } else {
            commissions = _commissionDBRepository.findByCommissionsByUser(id);
        }

        for (var commission : commissions) {
            CommissionReportDTO commissionReportDTO = new CommissionReportDTO(
                    commission.getAmount(),
                    commission.getBuyer(),
                    commission.getAddress(),
                    commission.getPrice(),
                    commission.getCity(),
                    commission.getNeigh(),
                    commission.getName(),
                    commission.getLastName()
            );
            comissionsReport.add(commissionReportDTO);
        }

        return comissionsReport;
    }
}
