package com.alianza.persistence;

import com.alianza.domain.dto.NeighbourhoodDTO;
import com.alianza.domain.repository.INeighbourhoodRepository;
import com.alianza.persistence.entity.Neighbourhood;
import com.alianza.persistence.mapper.NeighbourhoodMapper;
import com.alianza.persistence.operationsDB.NeighbourhoodDBRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository
public class NeighbourhoodRepository implements INeighbourhoodRepository {
    private final NeighbourhoodDBRepository _repository;
    private final NeighbourhoodMapper _mapper;

    @Autowired
    public NeighbourhoodRepository(NeighbourhoodDBRepository repository, NeighbourhoodMapper mapper) {
        _repository = repository;
        _mapper = mapper;
    }

    @Override
    public List<NeighbourhoodDTO> getAllByCity(String idCity) {
        List<NeighbourhoodDTO> neighbourhoodsCity = (List<NeighbourhoodDTO>) _repository.findByIdCity(idCity).map(neighbourhoods -> {
            return _mapper.toNeighbourhoodsDTO(neighbourhoods);
        }).orElse(new ArrayList<>());

        return neighbourhoodsCity;
    }
}
