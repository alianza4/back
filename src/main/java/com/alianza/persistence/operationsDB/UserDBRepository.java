package com.alianza.persistence.operationsDB;

import com.alianza.persistence.entity.User;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;

public interface UserDBRepository extends CrudRepository<User, String> {
    Optional<User> findByEmailAndPassword(String email, String password);

    Optional<List<User>> findByIdRol(String idRol);
}
