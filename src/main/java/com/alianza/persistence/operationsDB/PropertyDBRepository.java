package com.alianza.persistence.operationsDB;

import com.alianza.persistence.entity.Property;
import org.springframework.data.repository.CrudRepository;

public interface PropertyDBRepository extends CrudRepository<Property, String> {

}
