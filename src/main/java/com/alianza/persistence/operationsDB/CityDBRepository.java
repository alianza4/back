package com.alianza.persistence.operationsDB;

import com.alianza.persistence.entity.City;
import org.springframework.data.repository.CrudRepository;

public interface CityDBRepository extends CrudRepository<City, String> {
}
