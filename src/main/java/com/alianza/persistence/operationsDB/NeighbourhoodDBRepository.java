package com.alianza.persistence.operationsDB;

import com.alianza.persistence.entity.Neighbourhood;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;

public interface NeighbourhoodDBRepository extends CrudRepository<Neighbourhood, String> {
    Optional<List<Neighbourhood>> findByIdCity(String idCity);
}
