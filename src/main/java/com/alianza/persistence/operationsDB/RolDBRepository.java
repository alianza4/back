package com.alianza.persistence.operationsDB;

import com.alianza.persistence.entity.Rol;
import org.springframework.data.repository.CrudRepository;

public interface RolDBRepository extends CrudRepository<Rol, String> {
}
