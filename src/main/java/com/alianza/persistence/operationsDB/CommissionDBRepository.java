package com.alianza.persistence.operationsDB;

import com.alianza.domain.dto.CommissionReportDTO;
import com.alianza.persistence.entity.Commission;
import com.alianza.utils.ICommissionProjection;
import com.alianza.utils.Queries;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Optional;

public interface CommissionDBRepository extends CrudRepository<Commission, String> {
    Optional<List<Commission>> findByIdUser(String idUser);

    @Query(value = Queries.commissions, nativeQuery = true)
    List<ICommissionProjection> findByCommissionsAll();

    @Query(value = Queries.commissionsByUser, nativeQuery = true)
    List<ICommissionProjection> findByCommissionsByUser(@Param("id") String id);
}
