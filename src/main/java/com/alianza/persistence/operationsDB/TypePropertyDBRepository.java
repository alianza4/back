package com.alianza.persistence.operationsDB;

import com.alianza.persistence.entity.TypeProperty;
import org.springframework.data.repository.CrudRepository;

public interface TypePropertyDBRepository extends CrudRepository<TypeProperty, String> {
}
