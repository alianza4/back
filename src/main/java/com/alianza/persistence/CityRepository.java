package com.alianza.persistence;

import com.alianza.domain.dto.CityDTO;
import com.alianza.domain.repository.ICityRepository;
import com.alianza.persistence.entity.City;
import com.alianza.persistence.mapper.CityMapper;
import com.alianza.persistence.mapper.TypePropertyMapper;
import com.alianza.persistence.operationsDB.CityDBRepository;
import com.alianza.persistence.operationsDB.TypePropertyDBRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class CityRepository implements ICityRepository {
    private final CityDBRepository _repository;
    private final CityMapper _mapper;

    @Autowired
    public CityRepository(CityDBRepository repository, CityMapper mapper) {
        _repository = repository;
        _mapper = mapper;
    }

    @Override
    public List<CityDTO> getAll() {
        List<City> cities = (List<City>) _repository.findAll();
        return _mapper.toCitiesDTO(cities);
    }
}
