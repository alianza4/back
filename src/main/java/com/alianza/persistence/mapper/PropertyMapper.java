package com.alianza.persistence.mapper;

import com.alianza.domain.dto.PropertyDTO;
import com.alianza.persistence.entity.Property;
import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring",
        uses = {CityMapper.class,
                NeighbourhoodMapper.class,
                ImageMapper.class,
                TypePropertyMapper.class,
                UserMapper.class,
                UserPropertyMapper.class})
public interface PropertyMapper {
    PropertyDTO toPropertyDTO(Property property);

    List<PropertyDTO> toPropertiesDTO(List<Property> properties);

    @InheritInverseConfiguration
    Property toProperty(PropertyDTO propertyDTO);
}
