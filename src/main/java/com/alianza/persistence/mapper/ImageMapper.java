package com.alianza.persistence.mapper;

import com.alianza.domain.dto.ImageDTO;
import com.alianza.persistence.entity.Image;
import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.List;

@Mapper(componentModel = "spring", uses = {PropertyMapper.class})
public interface ImageMapper {
    ImageDTO toImageDTO(Image image);
    List<ImageDTO> toImagesDTO(List<Image> images);

    @InheritInverseConfiguration
    @Mapping(target = "properties", ignore = true)
    Image toImage(ImageDTO imageDTO);
}
