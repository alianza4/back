package com.alianza.persistence.mapper;

import com.alianza.domain.dto.NeighbourhoodDTO;
import com.alianza.persistence.entity.Neighbourhood;
import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

import java.util.List;

@Mapper(componentModel = "spring", uses = {CityMapper.class, PropertyMapper.class})
public interface NeighbourhoodMapper {
    NeighbourhoodDTO toNeighbourhoodDTO(Neighbourhood neighbourhood);

    List<NeighbourhoodDTO> toNeighbourhoodsDTO(List<Neighbourhood> neighbourhoods);

    @InheritInverseConfiguration
    @Mappings({
            @Mapping(target = "city", ignore = true),
            @Mapping(target = "properties", ignore = true),
            @Mapping(target = "idCity", ignore = true)
    })
    Neighbourhood toNeighbourhood(NeighbourhoodDTO neighbourhoodDTO);
}
