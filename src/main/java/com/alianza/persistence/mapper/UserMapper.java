package com.alianza.persistence.mapper;

import com.alianza.domain.dto.UserDTO;
import com.alianza.persistence.entity.User;
import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

import java.util.List;

@Mapper(componentModel = "spring", uses = {RolMapper.class, PropertyMapper.class, UserPropertyMapper.class})
public interface UserMapper {
    UserDTO toUserDTO(User user);

    List<UserDTO> toUsersDTO(List<User> users);

    @InheritInverseConfiguration
    User toUser(UserDTO userDTO);
}
