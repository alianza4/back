package com.alianza.persistence.mapper;

import com.alianza.domain.dto.UserPropertyDTO;
import com.alianza.persistence.entity.UserProperty;
import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

@Mapper(componentModel = "spring", uses = {UserMapper.class, PropertyMapper.class})
public interface UserPropertyMapper {
    @Mappings({
            @Mapping(source = "id.idUser", target = "idUser"),
            @Mapping(source = "id.idProperty", target = "idProperty")
    })
    UserPropertyDTO toUserPropertyDTO(UserProperty userProperty);

    @InheritInverseConfiguration
    @Mappings({
            @Mapping(target = "user", ignore = true),
            @Mapping(target = "property", ignore = true)
    })
    UserProperty toUserProperty(UserPropertyDTO userPropertyDTO);
}
