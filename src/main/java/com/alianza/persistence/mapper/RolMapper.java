package com.alianza.persistence.mapper;

import com.alianza.domain.dto.RolDTO;
import com.alianza.persistence.entity.Rol;
import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.List;

@Mapper(componentModel = "spring", uses = {UserMapper.class})
public interface RolMapper {
    RolDTO toRolDTO(Rol rol);

    List<RolDTO> toRolsDTO(List<Rol> rols);

    @InheritInverseConfiguration
    @Mapping(target = "users", ignore = true)
    Rol toRol(RolDTO rolDTO);
}
