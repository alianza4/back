package com.alianza.persistence.mapper;

import com.alianza.domain.dto.CityDTO;
import com.alianza.persistence.entity.City;
import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

import java.util.List;

@Mapper(componentModel = "spring", uses = {NeighbourhoodMapper.class})
public interface CityMapper {
    CityDTO toCityDTO(City city);

    List<CityDTO> toCitiesDTO(List<City> cities);

    @InheritInverseConfiguration
    @Mappings({
            @Mapping(target = "properties", ignore = true),
            @Mapping(target = "neighbourhoods", ignore = true)
    })
    City toCity(CityDTO cityDTO);
}
