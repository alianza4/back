package com.alianza.persistence.mapper;

import com.alianza.domain.dto.CommissionDTO;
import com.alianza.persistence.entity.Commission;
import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

import java.util.List;

@Mapper(componentModel = "spring", uses = {UserMapper.class})
public interface CommissionMapper {
    CommissionDTO toCommissionDTO(Commission commission);

    List<CommissionDTO> toCommissionsDTO(List<Commission> commissions);

    @InheritInverseConfiguration
    @Mappings({
            @Mapping(target = "user", ignore = true)
    })
    Commission toCommission(CommissionDTO commissionDTO);
}
