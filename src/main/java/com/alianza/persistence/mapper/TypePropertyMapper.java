package com.alianza.persistence.mapper;

import com.alianza.domain.dto.TypePropertyDTO;
import com.alianza.persistence.entity.TypeProperty;
import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.List;

@Mapper(componentModel = "spring", uses = {PropertyMapper.class})
public interface TypePropertyMapper {
    TypePropertyDTO toTypePropertyDTO(TypeProperty typeProperty);

    List<TypePropertyDTO> toTypePropertiesDTO(List<TypeProperty> typeProperties);

    @InheritInverseConfiguration
    @Mapping(target = "properties", ignore = true)
    TypeProperty toTypeProperty(TypePropertyDTO typePropertyDTO);
}
