package com.alianza.persistence;

import com.alianza.domain.dto.PropertyDTO;
import com.alianza.domain.dto.UserPropertyDTO;
import com.alianza.domain.repository.IPropertyRepository;
import com.alianza.persistence.entity.Property;
import com.alianza.persistence.entity.User;
import com.alianza.persistence.mapper.PropertyMapper;
import com.alianza.persistence.operationsDB.PropertyDBRepository;
import com.alianza.persistence.operationsDB.UserDBRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

@Repository
public class PropertyRepository implements IPropertyRepository {
    private final PropertyDBRepository _propertyDBRepository;
    private final PropertyMapper _mapper;
    private final UserDBRepository _userDBRepository;

    @Autowired
    public PropertyRepository(PropertyDBRepository propertyDBRepository, PropertyMapper mapper, UserDBRepository userDBRepository) {
        _propertyDBRepository = propertyDBRepository;
        _mapper = mapper;
        _userDBRepository = userDBRepository;
    }

    @Override
    public List<PropertyDTO> getAll() {
        List<Property> properties = (List<Property>) _propertyDBRepository.findAll();
        return _mapper.toPropertiesDTO(properties);
    }

    @Override
    public PropertyDTO get(String id) {
        PropertyDTO property = _propertyDBRepository.findById(id)
                .map(pro -> _mapper.toPropertyDTO(pro))
                .orElse(null);

        return property;
    }

    @Override
    public PropertyDTO save(PropertyDTO propertyDTO) {
        Property propertyExist = _propertyDBRepository.findById(propertyDTO.getId()).map(prop -> prop).orElse(null);

        if (propertyExist == null) {
            List<User> comercials = _userDBRepository.findByIdRol("41f1680d-f31e-48b7-8d29-18be803641ad").map(users -> {
                return users;
            }).orElse(null);

            Random random = new Random();
            var position = random.nextInt(comercials.size());
            User userComercial = comercials.get(position);

            var userProperties = propertyDTO.getUserProperties();
            userProperties.add(new UserPropertyDTO(userComercial.getId(), null));
            propertyDTO.setUserProperties(userProperties);

            Property property = _mapper.toProperty(propertyDTO);
            property.getUserProperties().forEach(up -> {
                up.setProperty(property);
            });
            Property propertyDB = _propertyDBRepository.save(property);
            return _mapper.toPropertyDTO(propertyDB);
        }

        propertyDTO.setUserProperties(new ArrayList<>());
        Property property = _mapper.toProperty(propertyDTO);
        Property propertyDB = _propertyDBRepository.save(property);
        return _mapper.toPropertyDTO(propertyDB);
    }

    @Override
    public boolean delete(String id) {
        if (get(id) != null) {
            _propertyDBRepository.deleteById(id);
            return true;
        }

        return false;
    }

    @Override
    public List<PropertyDTO> getPropertiesByUser(String id) {
        User user = _userDBRepository.findById(id).map(u -> u).orElse(null);
        if (user != null && user.getIdRol().equals("01fd1387-74a4-42ac-a51f-3b7e1d3328bd")) {
            return getAll();
        }

        if (user != null) {
            List<PropertyDTO> propertyDTOS = new ArrayList<>();
            user.getUserProperties().forEach(up -> {
                var propertyDTO = get(up.getId().getIdProperty());
                propertyDTOS.add(propertyDTO);
            });

            return propertyDTOS;
        }

        return null;
    }
}
