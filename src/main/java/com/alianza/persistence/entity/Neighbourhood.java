package com.alianza.persistence.entity;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "neighbourhood")
public class Neighbourhood {
    @Id
    private String id;
    private String description;
    @Column(name = "id_city")
    private String idCity;

    @ManyToOne
    @JoinColumn(name = "id_city", insertable = false, updatable = false)
    private City city;

    @OneToMany(mappedBy = "neighbourhood")
    private List<Property> properties;

    public City getCity() {
        return city;
    }

    public void setCity(City city) {
        this.city = city;
    }

    public List<Property> getProperties() {
        return properties;
    }

    public void setProperties(List<Property> properties) {
        this.properties = properties;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getIdCity() {
        return idCity;
    }

    public void setIdCity(String idCity) {
        this.idCity = idCity;
    }
}
