package com.alianza.persistence.entity;

import javax.persistence.*;

@Entity
@Table(name = "userproperty")
public class UserProperty {
    @EmbeddedId
    private UserPropertyPK id;

    @ManyToOne
    @JoinColumn(name = "id_user", insertable = false, updatable = false)
    private User user;

    @ManyToOne
    @MapsId("idProperty")
    @JoinColumn(name = "id_property", insertable = false, updatable = false)
    private Property property;

    public UserPropertyPK getId() {
        return id;
    }

    public void setId(UserPropertyPK id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Property getProperty() {
        return property;
    }

    public void setProperty(Property property) {
        this.property = property;
    }
}
