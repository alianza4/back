package com.alianza.persistence.entity;


import com.alianza.utils.OfferType;
import com.alianza.utils.StateProperty;

import javax.persistence.*;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import java.util.List;

@Entity
@Table(name = "property")
public class Property {
    @Id
    private String id;
    @Column(name = "offer_type")
    @Enumerated(EnumType.ORDINAL)
    private OfferType offerType;
    @Column(name = "id_type_property")
    private String idTypeProperty;
    @Column(name = "id_city")
    private String idCity;
    @Column(name = "id_neighbourhood")
    private String idNeighbourhood;
    private String address;
    private double price;
    @Min(1)
    @Max(6)
    private int stratum;
    private double area;
    @Column(name = "administration_in_price")
    private boolean administrationInPrice;
    private int rooms;
    private int bathrooms;
    private int floors;
    private String description;
    @Column(name = "id_image")
    private String idImage;
    private boolean parking;
    @Enumerated(EnumType.ORDINAL)
    private StateProperty state = StateProperty.OFFER;

    @ManyToOne
    @JoinColumn(name = "id_city", insertable = false, updatable = false)
    private City city;

    @ManyToOne
    @JoinColumn(name = "id_neighbourhood", insertable = false, updatable = false)
    private Neighbourhood neighbourhood;

    @ManyToOne
    @JoinColumn(name = "id_image", insertable = false, updatable = false)
    private Image image;

    @ManyToOne
    @JoinColumn(name = "id_type_property", insertable = false, updatable = false)
    private TypeProperty typeProperty;

    @OneToMany(mappedBy = "property", cascade = {CascadeType.ALL})
    private List<UserProperty> userProperties;

    public List<UserProperty> getUserProperties() {
        return userProperties;
    }

    public void setUserProperties(List<UserProperty> userProperties) {
        this.userProperties = userProperties;
    }

    public City getCity() {
        return city;
    }

    public void setCity(City city) {
        this.city = city;
    }

    public Neighbourhood getNeighbourhood() {
        return neighbourhood;
    }

    public void setNeighbourhood(Neighbourhood neighbourhood) {
        this.neighbourhood = neighbourhood;
    }

    public Image getImage() {
        return image;
    }

    public void setImage(Image image) {
        this.image = image;
    }

    public TypeProperty getTypeProperty() {
        return typeProperty;
    }

    public void setTypeProperty(TypeProperty typeProperty) {
        this.typeProperty = typeProperty;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public OfferType getOfferType() {
        return offerType;
    }

    public void setOfferType(OfferType offerType) {
        this.offerType = offerType;
    }

    public String getIdTypeProperty() {
        return idTypeProperty;
    }

    public void setIdTypeProperty(String idTypeProperty) {
        this.idTypeProperty = idTypeProperty;
    }

    public String getIdCity() {
        return idCity;
    }

    public void setIdCity(String idCity) {
        this.idCity = idCity;
    }

    public String getIdNeighbourhood() {
        return idNeighbourhood;
    }

    public void setIdNeighbourhood(String idNeighbourhood) {
        this.idNeighbourhood = idNeighbourhood;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public int getStratum() {
        return stratum;
    }

    public void setStratum(int stratum) {
        this.stratum = stratum;
    }

    public double getArea() {
        return area;
    }

    public void setArea(double area) {
        this.area = area;
    }

    public boolean isAdministrationInPrice() {
        return administrationInPrice;
    }

    public void setAdministrationInPrice(boolean administrationInPrice) {
        this.administrationInPrice = administrationInPrice;
    }

    public int getRooms() {
        return rooms;
    }

    public void setRooms(int rooms) {
        this.rooms = rooms;
    }

    public int getBathrooms() {
        return bathrooms;
    }

    public void setBathrooms(int bathrooms) {
        this.bathrooms = bathrooms;
    }

    public int getFloors() {
        return floors;
    }

    public void setFloors(int floors) {
        this.floors = floors;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getIdImage() {
        return idImage;
    }

    public void setIdImage(String idImage) {
        this.idImage = idImage;
    }

    public boolean isParking() {
        return parking;
    }

    public void setParking(boolean parking) {
        this.parking = parking;
    }

    public StateProperty getState() {
        return state;
    }

    public void setState(StateProperty state) {
        this.state = state;
    }
}
