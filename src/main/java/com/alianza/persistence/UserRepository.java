package com.alianza.persistence;

import com.alianza.domain.dto.RequestLoginDTO;
import com.alianza.domain.dto.RolDTO;
import com.alianza.domain.dto.UserDTO;
import com.alianza.domain.repository.IUserRepository;
import com.alianza.persistence.entity.Rol;
import com.alianza.persistence.entity.User;
import com.alianza.persistence.mapper.RolMapper;
import com.alianza.persistence.mapper.UserMapper;
import com.alianza.persistence.operationsDB.RolDBRepository;
import com.alianza.persistence.operationsDB.UserDBRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.concurrent.atomic.AtomicReference;

@Repository
public class UserRepository implements IUserRepository {
    private final UserDBRepository _userDBRepository;
    private final RolDBRepository _rolDBRepository;
    private final UserMapper _mapper;
    private final RolMapper _mapperRol;

    @Autowired
    public UserRepository(UserDBRepository userDBRepository, UserMapper userMapper, RolDBRepository rolDBRepository, RolMapper mapperRol) {
        _userDBRepository = userDBRepository;
        _rolDBRepository = rolDBRepository;
        _mapper = userMapper;
        _mapperRol = mapperRol;
    }

    @Override
    public List<UserDTO> getAll() {
        List<User> users = (List<User>) _userDBRepository.findAll();
        return _mapper.toUsersDTO(users);
    }

    @Override
    public UserDTO get(String id) {
        UserDTO user = _userDBRepository.findById(id).map(us -> _mapper.toUserDTO(us)).orElse(null);
        return user;
    }

    @Override
    public UserDTO save(UserDTO userDTO) {
        User user = _mapper.toUser(userDTO);
        User res = _userDBRepository.save(user);
        return _mapper.toUserDTO(res);
    }

    @Override
    public boolean delete(String id) {
        if (get(id) != null) {
            _userDBRepository.deleteById(id);
            return true;
        }

        return false;
    }

    @Override
    public UserDTO login(RequestLoginDTO requestLoginDTO) {
        UserDTO user = _userDBRepository.findByEmailAndPassword(requestLoginDTO.getEmail(), requestLoginDTO.getPassword())
                .map(us -> _mapper.toUserDTO(us))
                .orElse(null);

        return user;
    }

    @Override
    public List<RolDTO> getAllRoles() {
        List<Rol> roles = (List<Rol>) _rolDBRepository.findAll();
        return _mapperRol.toRolsDTO(roles);
    }

    @Override
    public UserDTO getUserComercial(List<String> ids) {
        AtomicReference<UserDTO> userDTO = new AtomicReference<>();
        List<User> users = (List<User>) _userDBRepository.findAllById(ids);
        users.forEach(user -> {
            if (user.getIdRol().equals("41f1680d-f31e-48b7-8d29-18be803641ad")) {
                userDTO.set(_mapper.toUserDTO(user));
            }
        });

        return userDTO.get();
    }
}
